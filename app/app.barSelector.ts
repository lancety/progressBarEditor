import {Component, Input, Output, EventEmitter} from "@angular/core";
import {BarObject} from "./app.bar";
@Component({
  selector: "bar-selector",
  template: `<select (change)="selectedBar($event)" class="form-control">
    <option *ngFor="let barObject of bars; let index = index" [value]="index">
      Progress Bar {{index + 1}} [{{barObject.value}} / {{barObject.limit}}]
    </option>
  </select>`
})

export class BarSelector {
  @Input() bars:BarObject[];
  @Output() selectBar:EventEmitter<number> = new EventEmitter();

  selectedBar(event:any) {
    this.selectBar.emit(parseInt(event.target.value));
  }
}
