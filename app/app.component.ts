import {Component} from '@angular/core';
import {IProgressBarEndPoint} from "./app.endpoint";

@Component({
  selector: 'my-app',
  template: `
    <h1 class="col-xs-12">Progress Bar Editor . 
      <small>
        <a href="http://www.lancety.net" target="_blank">Lance TIAN</a> . 
        <a href="https://gitlab.com/lancety/progressBarEditor" target="_blank">Gitlab</a></small></h1>
    <bar-editor class="col-xs-12"></bar-editor>
    `
})
export class AppComponent {
}
