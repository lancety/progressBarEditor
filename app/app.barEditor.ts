import {Component, Input} from "@angular/core";
import {ProgressBarEndPoint, IProgressBarEndPoint} from "./app.endpoint";
import {BarGroup} from "./app.bar";
import {BtnGroup} from "./app.barChangeBtn";


@Component({
  selector: "bar-editor",
  template: `
    <bar 
      *ngFor="let barObject of barGroup.bars" 
      [barObject]="barObject"
      class="col-xs-12"></bar>
    <bar-selector 
      [bars]="barGroup.bars" 
      (selectBar)="selectBar($event)"
      class="col-xs-12 col-md-3"></bar-selector>
    <div class="btn-container col-xs-12 col-md-9">
      <bar-change-btn 
        *ngFor="let btnValue of btnGroup.btns" 
        [btnValue]="btnValue" 
        (btnClicked)="updateBarValue($event)"></bar-change-btn>
    </div>
    `
})

export class BarEditor {
  private endPoint: ProgressBarEndPoint ;
  private barGroup: BarGroup;
  private btnGroup: BtnGroup;


  private activeBarIndex: number = 0;

  ngOnInit() {
    this.endPoint = new ProgressBarEndPoint();
    this.barGroup = new BarGroup(this.endPoint.bars, this.endPoint.limit);
    this.btnGroup = new BtnGroup(this.endPoint.buttons);
  }

  selectBar(index: number){
    this.activeBarIndex = index;
    console.log(index)
  }

  updateBarValue(value: number){
    this.barGroup.updateBarValue(this.activeBarIndex, value);
  }
}

