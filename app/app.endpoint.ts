export const generatorConfig = {
  minBar: 4,
  maxBar: 6,
  minBarValue: 0,
  maxBarValue: 100,
  minButton: 2,
  maxButton: 5,
  minButtonValue: 10,
  maxButtonValue: 50,
  minLimit: 110,
  maxLimit: 230
};

export interface IProgressBarEndPoint {
  buttons:number[];
  bars:number[];
  limit:number;
}

export class ProgressBarEndPoint {
  static minBar = 4;
  static maxBar = 6;

  bars:number[] = [];
  buttons:number[] = [];
  limit:number;

  constructor() {
    this.limit = ProgressBarEndPoint.generateRandomNumberArray(
      generatorConfig.minLimit,
      generatorConfig.maxLimit
    );
    this.bars = ProgressBarEndPoint.generateBars();
    this.buttons = ProgressBarEndPoint.generateButtons();
  }

  static generateRandomNumberArray(rangeMin:number, rangeMax:number):number {
    return Math.floor(Math.random() * (rangeMax - rangeMin + 1) + rangeMin);
  }

  static generateBars():number[] {
    let barValueError:number[] = [];
    for (let i = 0;
         i < ProgressBarEndPoint.generateRandomNumberArray(generatorConfig.minBar, generatorConfig.maxBar);
         i++
    ) {
      barValueError.push(ProgressBarEndPoint.generateRandomNumberArray(
        generatorConfig.minBarValue,
        generatorConfig.maxBarValue
      ))
    }

    return barValueError;
  }

  static generateButtons():number[] {
    let hasPositive = false;
    let hasNegative = false;
    let btnValueError:number[] = [];
    for (let i = 0;
         i < ProgressBarEndPoint.generateRandomNumberArray(generatorConfig.minButton, generatorConfig.maxButton);
         i++
    ) {
      let tmpNumber:number = ProgressBarEndPoint.generateRandomNumberArray(
        generatorConfig.minButtonValue,
        generatorConfig.maxButtonValue
      );

      if (Math.floor(Math.random() * 2) === 0) {
        btnValueError.push(tmpNumber)
        hasPositive = true;
      } else {
        btnValueError.push(0 - tmpNumber);
        hasNegative = true;
      }
    }

    // make sure there always has both positive and negative value for buttons
    if (!hasPositive || !hasNegative) {
      btnValueError[0] = 0 - btnValueError[0]
    }

    ProgressBarEndPoint.sortNumberAsc(btnValueError);
    return btnValueError;
  }

  static sortNumberAsc(numberArray:number[]) {
    return numberArray.sort((a, b)=>a - b);
  }
}
