import {Component, Input, Output, EventEmitter} from "@angular/core";
@Component({
  selector: "bar-change-btn",
  template: `<button (click)="updateBarValue(btnValue)" value="btnValue" class="btn btn-default">{{btnValueString()}}</button>`
})

export class BarChangeBtn {
  @Input() btnValue:number;
  @Output() btnClicked:EventEmitter<number> = new EventEmitter();

  updateBarValue(btnValue:number) {
    this.btnClicked.emit(btnValue);
  }

  btnValueString():string {
    return this.btnValue > 0 ? "+"+this.btnValue : String(this.btnValue);
  }
}

export class BtnGroup {
  constructor(public btns:number[]) {
  }
}
