import {Component, Input} from "@angular/core";
@Component({
  selector: "bar",
  template: `<div class="progress">
    <div 
      class="progress-bar" 
      [ngStyle]="{'width': styleWidth()+'%'}" 
      [ngClass]="{'over-limit': barObject.overLimit}"></div>
    <div class="percentageLayer">{{actualPercentage()}}%</div>
  </div>`
})

export class Bar {
  @Input() barObject:BarObject;

  styleWidth():number {
    if (this.barObject.overLimit) {
      return 100;
    }

    return this.actualPercentage();
  }

  actualPercentage(): number{
    return Math.floor(this.barObject.value / this.barObject.limit * 100)
  }
}

export class BarObject {
  public overLimit:boolean = false;

  constructor(public value:number, public limit:number) {
    this.value = this.value < 0 ? 0 : this.value;
  }

  updateValue(value:number) {
    let tmpValue:number = this.value + value;
    if (tmpValue < 0) {
      this.value = 0;
      console.error("lower than 0, auto set as 0");
    } else {
      this.value = tmpValue;
      this.overLimit = tmpValue > this.limit;
    }
  }
}

export class BarGroup {
  bars:BarObject[] = [];

  constructor(private barValue:number[], private limit:number) {
    for (let value of barValue) {
      this.bars.push(new BarObject(value, limit));
    }
  }

  updateBarValue(index:number, value:number) {
    if (index < this.bars.length) {
      this.bars[index].updateValue(value);
    } else {
      console.error("Can not find bar by index " + index);
    }
  }
}
