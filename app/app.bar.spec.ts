import {BarGroup, BarObject} from "./app.bar";
describe("Bar class test", () => {
  let bars:number[] = [30, 20, 10, 1];
  let limit:number = 210;

  it("should create correct number of bar object with given object", ()=> {
    let barGroup:BarGroup = new BarGroup(bars, limit);
    expect(barGroup.bars.length).toEqual(bars.length);
  });

  it("should create a bar object with value and limit", () => {
    let barObject:BarObject = new BarObject(10, 200);
    expect(barObject.overLimit).toBeFalsy();
    expect(barObject.value).toEqual(10);
    expect(barObject.limit).toEqual(200);
    barObject.updateValue(-20);
    expect(barObject.value).toEqual(0);
    barObject.updateValue(300);
    expect(barObject.value).toEqual(300);
    expect(barObject.overLimit).toBeTruthy();
    barObject.updateValue(-200);
    expect(barObject.value).toEqual(100);
    expect(barObject.overLimit).toBeFalsy();
  });

  it("should update given index bar object's value", ()=> {
    let barGroup:BarGroup = new BarGroup(bars, limit);
    barGroup.updateBarValue(100, 10);
    // nothing happens, but a console error message say cant find index 100 bar object.
    expect(barGroup.bars[0].value).toEqual(30);
    barGroup.updateBarValue(0, -1);
    expect(barGroup.bars[0].value).toEqual(29);
  });
});
