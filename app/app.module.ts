import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent }  from './app.component';

import {Bar} from "./app.bar";
import {BarSelector} from "./app.barSelector";
import {BarChangeBtn} from "./app.barChangeBtn";
import {BarEditor} from "./app.barEditor";


@NgModule({
  imports: [ BrowserModule ],
  declarations: [ AppComponent, BarEditor, Bar, BarSelector, BarChangeBtn],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
