import {IProgressBarEndPoint, ProgressBarEndPoint, generatorConfig} from "./app.endpoint";
describe("ProgressBarEndPoint class test", () => {
  let endPointObj = {"buttons": [30, 42, -12, -13], "bars": [88, 32, 25, 72], "limit": 210};

  it("should create end point class with given end point json", ()=> {
    let endPointInstance = endPointObj as IProgressBarEndPoint;
    expect(endPointInstance.buttons).toBe(endPointObj.buttons);
    expect(endPointInstance.bars).toBe(endPointObj.bars);
    expect(endPointInstance.limit).toEqual(endPointObj.limit);
  });

  it("should generate random number base on given configuration", ()=> {
    let result:number = ProgressBarEndPoint.generateRandomNumberArray(4, 5);
    expect(result === 4 || result === 5).toBeTruthy();
  });

  it("should generate correct number of bars", ()=> {
    let barArray:number[] = ProgressBarEndPoint.generateBars();
    for (let i = 0; i < 50; i++) {
      expect(barArray.length >= generatorConfig.minBar && barArray.length <= generatorConfig.maxBar).toBeTruthy();
    }
  });

  it("should generate correct number of buttons", ()=> {
    let btnArray:number[] = ProgressBarEndPoint.generateButtons();
    for (let i = 0; i < 50; i++) {
      expect(btnArray.length >= generatorConfig.minButton && btnArray.length <= generatorConfig.maxButton).toBeTruthy();
    }
  });

  it("should generate both positive and negative button value, also within range", () => {
    let btnArray:number[] = ProgressBarEndPoint.generateButtons();
    let hasPositive = false;
    let hasNegative = false;

    for (let i = 0; i < btnArray.length; i++) {
      if (hasPositive === false && btnArray[i] > 0) {
        hasPositive = true;
      } else if (hasNegative === false && btnArray[i] < 0) {
        hasNegative = true;
      }

      expect(
        btnArray[i] >= 0 - generatorConfig.maxButtonValue &&
        btnArray[i] <= generatorConfig.maxButtonValue
      ).toBeTruthy();
    }

    expect(hasPositive).toBeTruthy();
    expect(hasNegative).toBeTruthy();
  })
});
